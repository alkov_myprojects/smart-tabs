/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

/** Конструктор */
function Groups(callBackEventUpdate) {
    this.groups = '';
    this.callBackEventUpdate = callBackEventUpdate;
    chrome.storage.onChanged.addListener(function(changes, namespace) {
        if(
            typeof changes['automatic_multiple_tabs_v2'] !== 'undefined' &&
            namespace === 'local'
        ) {
            Groups.groups = changes['automatic_multiple_tabs_v2'].newValue;
            if(Groups.callBackEventUpdate) {
                Groups.callBackEventUpdate();
            }
        }
    });
}

/** Методы */
Groups.prototype.init = function (callBack) {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        var groups = data.automatic_multiple_tabs_v2;
        if (!Array.isArray(groups)) { groups = []; }
        Groups.groups = groups;
        callBack();
    });
};
Groups.prototype.clear = function () {
    chrome.storage.local.set({ 'automatic_multiple_tabs_v2': [] });
};
Groups.prototype.CheckWinID = function(win_id,callBack) {
    //chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        //var groups_in_storage = data.automatic_multiple_tabs_v2;
        //if(!Array.isArray(groups_in_storage)) { groups_in_storage = []; }
        var value_callBack = false;
        if(this.groups.length) {
            for ( var i = 0; i < this.groups.length; i++) {
                if(this.groups[i].window.id === win_id) {
                    value_callBack = true;
                }
            }
        }
        callBack(value_callBack);
    //});
};

Groups.prototype.equalTabs = function (tabs_1,tabs_2) {

    if(tabs_1.length !== tabs_2.length) { return false; }

    for( var i = 0; i < tabs_1.length; i++) {
        if(
            tabs_1[i]['pinned'] !== tabs_2[i]['pinned'] ||
            tabs_1[i]['selected'] !== tabs_2[i]['selected'] ||
            tabs_1[i]['url'] !== tabs_2[i]['url']
        ) {
            return false;
        }
    }

    return true;
};

Groups.prototype.generateHtml = function (callBack) {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {

        var groups = data.automatic_multiple_tabs_v2;
        if(!Array.isArray(groups)) { groups = []; }
    
        chrome.windows.getCurrent(function(win) {
            
            chrome.windows.getAll({populate : true},function (window_list) {

                var html = '';
                for ( var i = 0; i < groups.length; i++ ) {

                    html += '<a href="#" data-group-id="' + i + '" class="list-group-item list-group-item-action';

                    //region status
                    var status = ' list-group-item-secondary'; // закрытая
                    for( var g = 0; g < window_list.length; g++ ) {
                        if(window_list[g].id === groups[i].window.id) {
                            if(win.id === window_list[g].id) {
                                status = ' list-group-item-primary'; // текущая
                            } else {
                                status = ''; // открыта
                            }
                            break;
                        }
                    }
                    //endregion

                    //region html += ...
                    html += status + '" >' +
                        '<div class="body_item_group">' +
                        '   <div class="name">' + groups[i].name + '</div>' +
                        '   <div class="control align-middle">' +
                        //'<i class="fas fa-external-link-alt fa-rotate-180" title="Открыть в текущем окне"></i> ' +
                        '<i class="fas fa-external-link-alt open-new-wind" title="Открыть в новом окне"></i> ' +
                        '<i class="far fa-edit" title="Редактировать"></i> ' +
                        '<i class="far fa-trash-alt" title="Удалить"></i>' +
                        '   </div>' +
                        '</div>';
                    html += '</a>';
                    //endregion

                }
                if(html) {
                    callBack(html);
                }
            });
        });
    });
};
Groups.prototype.generateGroupByWindow = function(window,callBack) {

        var small_window = {};
        small_window.id = window.id;
        small_window.height = window.height;
        small_window.width = window.width;
        small_window.tabs = [];

        var tabs = window.tabs;
        for( var g = 0; g < tabs.length; g++) {
            small_window.tabs.push({
                'title': tabs[g].title,       // Заголовок
                'index': tabs[g]['index'],    // Индекс
                'pinned': tabs[g].pinned,     // Закреплена
                'selected': tabs[g].selected, // Выделен
                'url': tabs[g].url            // Ссылка
            });
        }

        callBack({ 'name': '', 'window': small_window });
};

Groups.prototype.RemoveGroupByID = function (group_id,callBack) {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        var groups = data.automatic_multiple_tabs_v2;
        if (!Array.isArray(groups)) { groups = []; }
        var groups_new = [];
        for ( var i = 0; i < Groups.groups.length; i++) {
            if( i !== group_id ) {
                groups_new.push(groups[i]);
            }
        }
        Groups.groups = groups_new;
        //callBack();
        chrome.storage.local.set({'automatic_multiple_tabs_v2': groups_new},callBack);
    });
};

Groups.prototype.refresh = function () {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        var groups_in_storage = data.automatic_multiple_tabs_v2;
        if(!Array.isArray(groups_in_storage)) { groups_in_storage = []; }
        chrome.storage.local.set({ 'automatic_multiple_tabs_v2': groups_in_storage });

    });
};

// Set
Groups.prototype.Import = function(data,callBack) {
    chrome.storage.local.set({ 'automatic_multiple_tabs_v2': data }, callBack);
};
Groups.prototype.AddNewGroup = function(win_id,name,callBack) {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {

        var groups_in_storage = data.automatic_multiple_tabs_v2;
        if(!Array.isArray(groups_in_storage)) { groups_in_storage = []; }

        chrome.windows.getAll({populate : true}, function (windows) {
            for(var i=0;i<windows.length;i++) {
                if(windows[i].id === win_id) {

                    var small_window = {};
                    small_window.id = windows[i].id;
                    small_window.height = windows[i].height;
                    small_window.width = windows[i].width;
                    small_window.tabs = [];

                    var tabs = windows[i].tabs;
                    for( var g = 0; g < tabs.length; g++) {
                        small_window.tabs.push({
                            'title': tabs[g].title,       // Заголовок
                            'index': tabs[g]['index'],    // Индекс
                            'pinned': tabs[g].pinned,     // Закреплена
                            'selected': tabs[g].selected, // Выделен
                            'url': tabs[g].url            // Ссылка
                        });
                    }

                    groups_in_storage.push({ 'name': name, 'window': small_window });
                    chrome.storage.local.set(
                        { 'automatic_multiple_tabs_v2': groups_in_storage },
                        callBack( groups_in_storage.length - 1 )
                    );
                }
            }
        });

    });
};
Groups.prototype.SetNameGroupByID = function (id,name,callBack) {
    for ( var i = 0; i < Groups.groups.length; i++) {
        if(i === id) {
            Groups.groups[id].name = name;
            chrome.storage.local.set( { 'automatic_multiple_tabs_v2': Groups.groups }, callBack );
        }
    }
};
Groups.prototype.SetWinID_ByGroupID = function (id,win_id,callBack) {
    for ( var i = 0; i < Groups.groups.length; i++) {
        if(i === id) {
            Groups.groups[id].window.id = win_id;
            chrome.storage.local.set( { 'automatic_multiple_tabs_v2': Groups.groups }, callBack );
        }
    }
};
Groups.prototype.SetGroupByID = function (group_id,data_new,callBack) {
    //chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        //var groups = data.automatic_multiple_tabs_v2;
        //if (!Array.isArray(groups)) { groups = []; }
        Groups.groups[group_id] = data_new;
        chrome.storage.local.set({'automatic_multiple_tabs_v2': Groups.groups},callBack);
    //});
};

// Get
Groups.prototype.GetStatus = function (group_id,callBack) {
    //chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {

        //var groups = data.automatic_multiple_tabs_v2;
        //if(!Array.isArray(groups)) { groups = []; }

        chrome.windows.getCurrent(function(win) {
            chrome.windows.getAll({populate : true},function (window_list) {

                //region status
                var status_find = false; // закрытая
                for( var g = 0; g < window_list.length; g++ ) {
                    if(window_list[g].id === Groups.groups[group_id].window.id) {
                        if(win.id === window_list[g].id) {
                            callBack('current'); // текущая
                        } else {
                            callBack('open'); // открыта
                        }
                        status_find = true;
                        break;
                    }
                }
                if(!status_find) { callBack('close'); }
                //endregion

            });
        });
    //});
};
Groups.prototype.GetAllGroups = function (callBack) {
    chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        var groups = data.automatic_multiple_tabs_v2;
        if (!Array.isArray(groups)) { groups = []; }
        callBack(groups);
    });
};
Groups.prototype.GetGroupByID = function (group_id,callBack) {
    //chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
        //var groups = data.automatic_multiple_tabs_v2;
        //if (!Array.isArray(groups)) { groups = []; }
        for( var i = 0; i < Groups.groups.length; i++ ) {
            if(i === group_id) {
                callBack(Groups.groups[i]);
            }
        }
    //});
};
Groups.prototype.GetGroupByWindowID = function (window_id,callBack) {
    //chrome.storage.local.get(['automatic_multiple_tabs_v2'], function(data) {
    //var groups = data.automatic_multiple_tabs_v2;
    //if (!Array.isArray(groups)) { groups = []; }
    for( var i = 0; i < Groups.groups.length; i++ ) {
        if(Groups.groups[i].window.id === window_id) {
            callBack(i,Groups.groups[i]);
        }
    }
    //});
};
