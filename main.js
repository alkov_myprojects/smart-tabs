/** ----------- Init ----------- */

var Groups = new Groups(function () {
    SetGenHtml();
});
Groups.init(function () {
    SetGenHtml();
});

function SetGenHtml() {
    Groups.generateHtml(function (html) {
        $('.none_groups').css('display', 'none');
        $('.list-group').css('display', 'block').html(html);
        $('.list-group .list-group-item').click(function () {
            var group_id = $(this).attr('data-group-id');
            clickByGroup(group_id);
        });
        $('.open-new-wind').click(function () {
            var group_id = parseInt($(this).parents().eq(2).attr('data-group-id'));
            openGroupInNewWindows(group_id);
            return false;
        });
        $('.fa-trash-alt').click(function () {
            var group_id = parseInt($(this).parents().eq(2).attr('data-group-id'));
            Groups.RemoveGroupByID(group_id, function () {
                document.location.reload();
            });
            return false;
        });
        $('.fa-edit').click(function () {
            var group_id = parseInt($(this).parents().eq(2).attr('data-group-id'));
            Groups.GetGroupByID(group_id, function (group) {
                $('.none_groups, .list-group, .add').css('display', 'none');
                $('.edit_group').css('display', 'block').find('input[name="GROUP_ID"]').val(group_id);
                $('.edit_group').find('input[name="GROUP_NAME"]').val(group.name).focus();
            });
            return false;
        });
    });
    chrome.windows.getCurrent(function (win) {
        Groups.CheckWinID(win.id, function (inGroup) {
            if (inGroup) {
                $('.add').css('display', 'none');
            }
        });
    });
}

$('.tooltip-on').tooltip({boundary: 'window'}); // всплывающая подсказка

/** ----------- User Interface ----------- */

//region История
$(".main-control .fa-buffer").click(function () {
    $(".front").css('transform', 'rotateY(180deg)');
    $(".back.history").css('transform', 'rotateY(360deg)');
});

$(".back .fa-sign-out-alt").click(function () {
    $(".front").css('transform', 'rotateY(0deg)');
    $(".back").css('transform', 'rotateY(180deg)');
});
//endregion

//region Добавление группы
$('.add i').click(function () {
    chrome.windows.getCurrent(function (win) {
        Groups.CheckWinID(win.id, function (inGroup) {
            if (inGroup) { // окно в группе => открывается пустое окно, с открытым приложением и формой ввода названия
                /*console.log('Алгоритм:');
                console.log('   - Создаём новое окно с пустой вкладкой');
                console.log('   - Добавляем это окно в группу');
                console.log('   - Открываем окно приложения');*/
            } else { // текущее окно браузера привязывается к новой группе + открывается форма ввода названия
                $('.edit_group').css('display', 'block').find('input[name="GROUP_ID"]').val('new');
                $('.none_groups, .list-group, .add').css('display', 'none');
            }
        });
    });
});

// Отправка формы редактирование названия группы
$('form.edit_group').submit(function () {

    var id = $(this).find('input[name="GROUP_ID"]').val();
    var name = $(this).find('input[name="GROUP_NAME"]').val();

    if (id === 'new') {
        chrome.windows.getCurrent(function (win) {
            Groups.AddNewGroup(win.id, name, function (new_group_id) {
                document.location.reload();
            });
        });
    } else {
        Groups.SetNameGroupByID(parseInt(id), name, function () {
            document.location.reload();
        });
    }

    return false;
});
$('form.edit_group [type=reset]').click(function () {
    $('.edit_group').css('display', 'none');
    $('.list-group, .add').css('display', 'block');
    chrome.windows.getCurrent(function (win) {
        Groups.CheckWinID(win.id, function (inGroup) {
            if (inGroup) {
                $('.add').css('display', 'none');
            }
        });
    });
});

$('.top-controll .fa-cog').hover(
    function () {
        $(this).addClass('fa-spin');
    },
    function () {
        $(this).removeClass('fa-spin');
    }
);
$('.fa-angle-left').click(function () {
    if (!$(this).hasClass('fa-angle-right')) {

        $(this).addClass('fa-angle-right');
        $(this).removeClass('fa-angle-left');

        $('.main-controll').animate({right: '300px'}, 400, function () {
        });
        $('.main-controll-box').animate({right: '0'}, 400, function () {
        });
        $(".bg_gray").css('z-index', '1').animate({opacity: 1}, 400, function () {
            // Animation complete.
        });

    } else {

        $(this).removeClass('fa-angle-right');
        $(this).addClass('fa-angle-left');

        $('.main-controll').animate({right: '0px'}, 400, function () {
        });
        $('.main-controll-box').animate({right: '-303px'}, 400, function () {
        });
        $(".bg_gray").animate({opacity: 0}, 400, function () {
            $(".bg_gray").css('z-index', '-1');
        });
    }
    // fa-rotate-180

    /*$( ".bg_gray" ).css('z-index','1').animate({
        opacity: 1
    }, 400, function() {
        // Animation complete.
    });*/
});
//endregion

//region Экспорт
$('.main-control .fa-file-export').click(function () {
    Groups.GetAllGroups(function (groups) {

        var groups_new = [];
        for (var i = 0; i < groups.length; i++) {

            var tabs = groups[i].window.tabs;

            for (var g = 0; g < tabs.length; g++) {
                var tab = tabs[g];
                if(typeof tab.title !== 'undefined') {
                    tab.title = utf8Encode(tab.title);
                }
                tabs[g] = tab;
            }
            groups[i].window.tabs = tabs;
            groups_new.push({
                'name': utf8Encode(groups[i].name),
                'window': groups[i].window
            });
        }

        var json_str = JSON.stringify(groups_new);
        var url = 'data:application/json;base64,' + btoa(json_str);
        chrome.downloads.download({
            url: url,
            filename: 'smart_tabs_export.json'
        });
    });
});

/**
 * Encodes multi-byte Unicode string into utf-8 multiple single-byte characters
 * (BMP / basic multilingual plane only).
 *
 * Chars in range U+0080 - U+07FF are encoded in 2 chars, U+0800 - U+FFFF in 3 chars.
 *
 * Can be achieved in JavaScript by unescape(encodeURIComponent(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} unicodeString - Unicode string to be encoded as UTF-8.
 * @returns {string} UTF8-encoded string.
 */
function utf8Encode(unicodeString) {
    if (typeof unicodeString != 'string') throw new TypeError('parameter ‘unicodeString’ is not a string');
    const utf8String = unicodeString.replace(
        /[\u0080-\u07ff]/g,  // U+0080 - U+07FF => 2 bytes 110yyyyy, 10zzzzzz
        function(c) {
            var cc = c.charCodeAt(0);
            return String.fromCharCode(0xc0 | cc>>6, 0x80 | cc&0x3f); }
    ).replace(
        /[\u0800-\uffff]/g,  // U+0800 - U+FFFF => 3 bytes 1110xxxx, 10yyyyyy, 10zzzzzz
        function(c) {
            var cc = c.charCodeAt(0);
            return String.fromCharCode(0xe0 | cc>>12, 0x80 | cc>>6&0x3F, 0x80 | cc&0x3f); }
    );
    return utf8String;
}

/**
 * Decodes utf-8 encoded string back into multi-byte Unicode characters.
 *
 * Can be achieved JavaScript by decodeURIComponent(escape(str)),
 * but this approach may be useful in other languages.
 *
 * @param   {string} utf8String - UTF-8 string to be decoded back to Unicode.
 * @returns {string} Decoded Unicode string.
 */
function utf8Decode(utf8String) {
    if (typeof utf8String != 'string') throw new TypeError('parameter ‘utf8String’ is not a string');
    // note: decode 3-byte chars first as decoded 2-byte strings could appear to be 3-byte char!
    const unicodeString = utf8String.replace(
        /[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,  // 3-byte chars
        function(c) {  // (note parentheses for precedence)
            var cc = ((c.charCodeAt(0)&0x0f)<<12) | ((c.charCodeAt(1)&0x3f)<<6) | ( c.charCodeAt(2)&0x3f);
            return String.fromCharCode(cc); }
    ).replace(
        /[\u00c0-\u00df][\u0080-\u00bf]/g,                 // 2-byte chars
        function(c) {  // (note parentheses for precedence)
            var cc = (c.charCodeAt(0)&0x1f)<<6 | c.charCodeAt(1)&0x3f;
            return String.fromCharCode(cc); }
    );
    return unicodeString;
}
//endregion

//region Импорт
$('.main-control .fa-file-import').click(function () {
    $('.back.import .inputData').css('display', 'block');
    $('.back.import .import__controll').css('display', 'none');

    $(".front").css('transform', 'rotateY(180deg)');
    $(".back.import").css('transform', 'rotateY(360deg)');
});
$('.top-control #downAll').click(function () {
    $( ".listElementsImport .elementImport" ).each(function( index, element ) {
        $(element).find('.elementImport__tabs').slideDown();
        $(element).find('.elementImport__title i.fas').removeClass('fa-angle-down').addClass('fa-angle-up');
    });
});
$('.top-control #upAll').click(function () {
    $( ".listElementsImport .elementImport" ).each(function( index, element ) {
        $(element).find('.elementImport__tabs').slideUp();
        $(element).find('.elementImport__title i.fas').removeClass('fa-angle-up').addClass('fa-angle-down');
    });
});
$('.import__controll #cancel_import').click(function () {
    $('.import__controll').fadeOut(function () {
        $('.inputData').fadeIn();
    });
});
$('.import__controll #start_import').click(function () {

    for (var i = 0; i < global_json.length; i++) {
        var item = global_json[i];
        item.window.id = '';
        global_json[i] = item;
    }
    
    Groups.Import(global_json,function () {

        //region При запуске браузера посмотреть текуще окна и найти в них дубликаты групп, если нашли то присвоить win_id
        chrome.windows.getAll({populate : true},function (window_list) {
            var groups = global_json; //Groups.groups;
            for(var i = 0; i < window_list.length; i++) {
                for(var id = 0; id < groups.length; id++) {
                    if(Groups.equalTabs(groups[id].window.tabs,window_list[i].tabs)) {
                        Groups.SetWinID_ByGroupID(id,window_list[i].id,function () {});
                    }
                }
            }
            $(".front").css('transform', 'rotateY(0deg)');
            $(".back").css('transform', 'rotateY(180deg)');
        });
        //endregion

    });
});

var global_json = [];
function handleFileSelect(evt) {

    var file = evt.target.files[0];

    //region errors
    var errors = '';
    if (!file) {
        errors = 'Вы не выбрали файл...';
    }
    if (file.name.split('.')[1] !== 'json') {
        errors = 'Рассширение файла не соответсвует фомату json';
    }
    //endregion

    var innerHTML = '';
    if (errors) {
        innerHTML = '<hr><b style="color:red">' + errors + '</b>';
    } else {
        fr = new FileReader();
        fr.onloadend = function () {

            var str = atob(fr.result.split(',')[1]); // decode
            var json = JSON.parse(str); // pasre to json

            var html = '';
            for (var i = 0; i < json.length; i++) {

                var item = json[i];

                //region Decode
                item.name = utf8Decode(item.name);
                for (var g = 0; g < item.window.tabs.length; g++) {
                    if(typeof item.window.tabs[g].title !== 'undefined') {
                        item.window.tabs[g].title = utf8Decode(item.window.tabs[g].title);
                    }
                }
                //endregion

                json[i] = item;

                //region html
                html += '<div class="elementImport">' +
                    '    <div class="elementImport__title">' +
                    '        ' + item.name + ' ( вкладок: <span class="elementImport__cntTabs">' + item.window.tabs.length + '</span> )' +
                    '        <i class="fas fa-angle-down"></i>' +
                    '    </div>' +
                    '    <div class="elementImport__tabs">';

                //region elementsImport
                for (var j = 0; j < item.window.tabs.length; j++) {

                    var elImport = item.window.tabs[j];

                    //region title
                    var title = 'No Title';
                    if(typeof elImport.title !== 'undefined') {
                        var newTitle = elImport.title;
                        title = newTitle.substring(0,25);
                        if(newTitle.length > 25) {
                            title += '...';
                        }
                    }
                    //endregion

                    html += '<div class="elementImport__tab">' +
                        '    <i class="fas fa-link tooltip-on mr-1"' +
                        '       data-toggle="tooltip"' +
                        '       data-placement="left"' +
                        '       title="' + elImport.url + '"' +
                        '    ></i>' +  title +
                        '</div>';
                }

                //endregion

                html += '</div></div>';
                //endregion
            }
            $('.listElementsImport').html(html);
            $('.listElementsImport .elementImport__title i.fas').click(function () {
                var parent = $(this).parents().eq(1);
                if($(this).hasClass('fa-angle-down')) {
                    $(parent).find('.elementImport__tabs').slideDown();
                    $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
                } else {
                    $(parent).find('.elementImport__tabs').slideUp();
                    $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
                }
            });

            global_json = json;

            $('.inputData').fadeOut(function () {
                $(this).find('input').val('');
                $('.import__controll').fadeIn();
            });

        };
        fr.readAsDataURL(file);
    }
    if(innerHTML) {
        document.getElementById('list').innerHTML = innerHTML;
    }
}
document.getElementById('file').addEventListener('change', handleFileSelect, false);
//endregion

//region Отладка
$('.main-control .fa-bug').click(function () {
    chrome.tabs.create({url: "hello.html"}); // открыть владку расширения
});
//endregion

//region Настройки
$('.main-control .fa-wrench').click(function () {
    //chrome.tabs.create({url: "hello.html"}); // открыть владку расширения
});
//endregion

/** ----------- Вспомогательные функции ----------- */

//region Клик по группе
function clickByGroup(group_id) {
    Groups.GetStatus(group_id, function (status) { // Какой статус группы?
        chrome.windows.getCurrent(function (currentWindows) { // Текущее окно привязано к группе?
            chrome.tabs.getAllInWindow(currentWindows.id, function (currentTabs) {
                Groups.CheckWinID(currentWindows.id, function (inGroup) { // Текущее окно привязано к группе?

                    //region status === 'close' && inGroup => Замена текущих вкладок на вкладки из группы
                    if (status === 'close' && inGroup) {

                        /**
                         * - Открыть новое окно с вкладками из группы
                         * - Закрыть старое окно
                         */
                        Groups.GetGroupByID(parseInt(group_id), function (group_new) {
                            Groups.GetGroupByWindowID(currentWindows.id, function (group_id_this, group_this) {

                                var new_win_id = currentWindows.id;
                                Groups.groups[group_id_this].window.id = false;

                                chrome.storage.local.set({'automatic_multiple_tabs_v2': Groups.groups}, function () {
                                    // установить флаг "Идёт загрузка группы"
                                    chrome.storage.local.set({'loadGroup': true}, function () {

                                            //region Воссоздание вкладок из выбранной группы
                                            var tabs = group_new.window.tabs;
                                            for (var i = 0; i < tabs.length; i++) {
                                                chrome.tabs.create({
                                                    'windowId': new_win_id,
                                                    'index': i,
                                                    'url': tabs[i].url,
                                                    'selected': tabs[i].selected,
                                                    'pinned': tabs[i].pinned
                                                });
                                            }
                                            if(typeof tabs.length === 'undefined' || tabs.length === 0) {
                                                chrome.tabs.create({'windowId': new_win_id, 'url': 'chrome://newtab/'});
                                            }
                                            //endregion

                                            //region удаление старых вкладок
                                            for (var g = 0; g < currentTabs.length; g++) {
                                                chrome.tabs.remove(currentTabs[g].id);
                                            }
                                            //endregion

                                            Groups.groups[group_id].window.id = new_win_id;
                                            chrome.storage.local.set({'automatic_multiple_tabs_v2': Groups.groups});

                                    });
                                });
                            });
                        });
                    }
                    //endregion

                    //region status === 'close' && !inGroup => Открыть вкладки из группы в новом окне
                    if (status === 'close' && !inGroup) {
                        openGroupInNewWindows(group_id);
                    }
                    //endregion

                    //region status === 'open' => Переключить фокус на открытое окно
                    if (status === 'open') {
                        Groups.GetGroupByID(parseInt(group_id), function (group) {
                            chrome.windows.update(group.window.id, {focused: true}); // переключить фокус
                            window.close(); // закрыть окно расширения
                        });
                    }
                    //endregion

                });
            });
        });
    });
}
//endregion

//region Открыть группу в новой вкладке
function openGroupInNewWindows(group_id) {
    chrome.windows.create(
        {'url': 'chrome://newtab/', state: "maximized"},
        function (window) {
            var tab_id_del = window.tabs[0].id; // получить id вспомогательной вкладки
            Groups.GetGroupByID(parseInt(group_id), function (group) {

                //region Воссоздание старых вкладок
                var tabs = group.window.tabs;
                for (var i = 0; i < tabs.length; i++) {
                    chrome.tabs.create({
                        'windowId': window.id,
                        'index': i,
                        'url': tabs[i].url,
                        'selected': tabs[i].selected,
                        'pinned': tabs[i].pinned
                    });
                }
                //endregion

                if (tabs.length) {
                    chrome.tabs.remove(tab_id_del); // Удалить вспомогательную вкладку
                }

                //region group update
                group.window.id = window.id;
                Groups.SetGroupByID(group_id, group, function () {
                });
                //endregion
            });
        }
    );
}
//endregion