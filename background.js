'use strict';
var Groups = new Groups(function () {
    //console.log('refresh data 2');
});
var init = false;
Groups.init(function () {
    //region При запуске браузера посмотреть текуще окна и найти в них дубликаты групп, если нашли то присвоить win_id
    chrome.windows.getAll({populate : true},function (window_list) {
        var groups = Groups.groups;
        for(var i = 0; i < window_list.length; i++) {
            for(var id = 0; id < groups.length; id++) {
                if(Groups.equalTabs(groups[id].window.tabs,window_list[i].tabs)) {
                    Groups.SetWinID_ByGroupID(id,window_list[i].id,function () {});
                }
            }
        }
    });
    //endregion
});

//region windows

//region onCreated
chrome.windows.onCreated.addListener(function (window) { // add
    //region при создании окна могли использовать клавиши восстановления ( Ctrl + Shift + T ) => проверить на дубликат
    chrome.windows.get(window.id, {populate:true}, function (window) { // получить текущее окно с вкладками
        var groups = Groups.groups;
        for(var id = 0; id < groups.length; id++) {
            if(Groups.equalTabs(groups[id].window.tabs,window.tabs)) { // у этого окна такие же вкладки как у группы
                console.log('onCreated');
                Groups.SetWinID_ByGroupID(id,window.id,function () {});
            }
        }
    });
    //endregion
});
//endregion

//region onRemoved
chrome.windows.onRemoved.addListener(function (windowId) { // del
    //region При закрытии окна, в других окнах могут быть открыты popup расшинеия, нужно вызвоть событие обновления данных, чтобы они обновились!!!
    Groups.CheckWinID(windowId,function (inGroup) {
        if(inGroup) {
            for (var i = 0; i < Groups.groups.length; i++) {
                if(Groups.groups[i].window.id === windowId) {
                    Groups.groups[i].window.id = false;
                    chrome.storage.local.set( { 'automatic_multiple_tabs_v2': Groups.groups } );
                    break;
                }
            }
        }
    });
    //endregion
});
//endregion

//endregion

//region tabs

function refreshByWinID(win_id,checkLenght) {
    setTimeout(function () {
        if(typeof win_id !== 'undefined') {
            Groups.CheckWinID(win_id,function (inGroup) {
                if(inGroup) { // окно вкладки входит группу
                    chrome.windows.getAll({populate : true},function (window_list) {
                        for (var i = 0; i < window_list.length; i++) {
                            var window_L = window_list[i];
                            if(window_L.id === win_id) {
                                if(typeof window_L !== 'undefined' && typeof window_L.id !== 'undefined') {
                                    console.log('refreshByWinID');
                                    Groups.GetGroupByWindowID(window_L.id,function (group_id,group) {
                                        if(checkLenght) {
                                            if(group.window.tabs.length === window_L.tabs.length) { // Это условие нужно, если очень быстро открыть/закрыть группу ( могут затереться вкладки... )
                                                if(!Groups.equalTabs(group.window.tabs,window_L.tabs)) { // у этого окна такие же вкладки как у группы
                                                    Groups.generateGroupByWindow(window_L,function (group_new) {
                                                        group_new.name = group.name;
                                                        Groups.SetGroupByID(group_id,group_new,function () {});
                                                    });
                                                }
                                            }
                                        } else {
                                            if(!Groups.equalTabs(group.window.tabs,window_L.tabs)) {
                                                Groups.generateGroupByWindow(window_L,function (group_new) {
                                                    group_new.name = group.name;
                                                    Groups.SetGroupByID(group_id,group_new,function () {});
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });
        }
    },100);
}

//region onUpdated
chrome.tabs.onUpdated.addListener(function (tabId,changeInfo,tab) {
    if(typeof tab.windowId !== 'undefined' && changeInfo.status === 'complete') {
        chrome.tabs.getAllInWindow(tab.windowId,function (tabs) {
            var allComplete = true;
            for (var i = 0; i < tabs.length; i++) {
                var item = tabs[i];
                if(item.status !== 'complete') {
                    allComplete = false;
                    break;
                }
            }
            if(allComplete) { // Вызывать только если это последнее загруженное окно
                refreshByWinID(tab.windowId);
            }
        });
    }
});
//endregion

//region onRemoved
chrome.tabs.onRemoved.addListener(function (tabId,removeInfo) {
    if(!removeInfo.isWindowClosing) {
        if(typeof removeInfo.windowId !== 'undefined') {
            //region Вызывать только если НЕ переход на другую группу
            chrome.storage.local.get(['loadGroup'], function(data) {
                if(typeof data.loadGroup === 'undefined' || data.loadGroup === false) {
                    refreshByWinID(removeInfo.windowId,true);
                } else {
                    setTimeout(function () {
                        chrome.storage.local.set({'loadGroup': false}); // Снять флаг "Идёт загрузка группы"
                    },100);
                }
            });
            //endregion
        }
    }
});
//endregion

//region onSelectionChanged
chrome.tabs.onSelectionChanged.addListener(function (tabId, selectInfo) {
    if(typeof selectInfo.windowId !== 'undefined') {
        console.log('onSelectionChanged');
        refreshByWinID(selectInfo.windowId);
    }
});
//endregion

//region onMoved
chrome.tabs.onMoved.addListener(function (tabId,moveInfo) {
    if(typeof moveInfo.windowId !== 'undefined') {
        console.log('onMoved');
        refreshByWinID(moveInfo.windowId);
    }
});
//endregion

//endregion