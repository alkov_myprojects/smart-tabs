var Groups = new Groups(function () {
    if(Groups.groups.length) {
        print_data(Groups.groups);
    } else {
        $('.groups').html('нет групп...');
    }
});
Groups.init(function () {
    if(Groups.groups.length) {
        print_data(Groups.groups);
    } else {
        $('.groups').html('нет групп...');
    }
});

function print_data(data) {

    //region .open_win
    chrome.windows.getAll({populate : true},function (windows) {
        $('.open_win .content').remove();

        var html = '';
        for (var i = 0; i < windows.length; i++) {
            var wind = windows[i];
            html += '<tr class="content" style="text-align: center">' +
                '    <td>' + wind.id + '</td>' +
                '    <td>' + wind.focused + '</td>' +
                '    <td>' + wind.tabs.length + '</td>' +
                '</tr>';
        }

        $('.open_win .title').after(html);
    });
    //endregion

    //region .groups
    var html = '';
    for( var i = 0; i < data.length; i++ ) {
        html += '<b>id: </b>' + i + '<br>';
        html += '<b>window.id: </b>' + data[i].window.id + '<br>';
        html += '<b>wtabs.length: </b>' + data[i].window.tabs.length + '<br>';
        html += '<b>name: </b>' + data[i].name + '<br>';
        html += '<b>Список ссылок: </b><br><div style="    overflow-y: auto;\n' +
            '    max-height: 100px;\n' +
            '    border-bottom: 1px solid;">';
        var tabs = data[i].window.tabs;
        for( var g = 0; g < tabs.length; g++ ) {
            html += '[' + g + '] ';

            //region title
            if(typeof tabs[g].title !== 'undefined') {
                html += tabs[g].title + ' | ';
            }
            //endregion

            //region url
            if(tabs[g].url.length > 130) {
                html += tabs[g].url.substring(0,130) + '<br>';
            } else {
                html += tabs[g].url + '<br>';
            }
            //endregion

        }
        html += '</div><br>';
    }
    $('.groups').html(html);
    //endregion
}

// var myid = chrome.runtime.id; // получить ID расширения
// chrome.tabs.create({url: "hello.html"}); // открыть владку расширения

/*
chrome.storage.onChanged.addListener(function(changes, namespace) {
    if(namespace === 'local') {
        var data = changes['automatic_multiple_tabs_v2'];
        console.log('Данные были изменены');
        console.log(data.newValue);
        print_data(data.newValue);
    }
});
*/
